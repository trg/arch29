#!/bin/sh

for pid in $(pidof -x "status_updater.sh"); do
    if [ $pid != $$ ]; then
        echo "$(date +"%F %T"): status_updater.sh is already running with PID $pid, killing"
        kill $pid
    fi
done

# Add an artificial sleep to wait for the IPC handler to be ready to process requests
sleep 0.5

SETSTATUS="duskc --ignore-reply run_command setstatus"

$SETSTATUS 7 "$(~/bin/statusbar/statusbutton)" &

secs=0
while true; do

    $SETSTATUS 0 "$(~/bin/statusbar/clock)" &
    $SETSTATUS 2 "$(~/bin/statusbar/mem)" &
    $SETSTATUS 3 "$(~/bin/statusbar/cpu)" &

    if [ $((secs % 60)) = 0 ]; then
        $SETSTATUS 5 "$(~/bin/statusbar/mouse_battery)" &
        $SETSTATUS 1 "$(~/bin/statusbar/volume)" &
    fi

    if [ $((secs % 3600)) = 0 ]; then
        $SETSTATUS 4 "$(~/bin/statusbar/sysupdates_paru)" &
    fi

    ((secs+=1))
    sleep 1
done
