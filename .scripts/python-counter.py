#!/usr/bin/env python3
# Initialize counter
counter = 1
# Iterate the loop 5 times
while counter < 30:
    # Print the counter value
    print ("The current counter value: %d" % counter)
    # Increment the counter
    counter = counter + 1
