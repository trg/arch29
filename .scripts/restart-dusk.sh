#!/bin/bash

duskc run_command restart &
last_pid=$!
sleep 1
kill -KILL $last_pid
