#!/bin/sh



intern=eDP1
extern=DP1-2




if xrandr | grep "$extern disconnected"; then
    xrandr --output "$extern" --off --output "$intern" --mode 1366x768 --primary --rate 60
else
    xrandr --output "$intern" --off --output "$extern" --mode 2560x1440 --primary --rate 60
fi

#Note: To leave the default monitor enabled when an external monitor is connected, replace the else clause with xrandr --output "$intern" --primary --auto --output "$extern" --right-of "$intern" --auto.
