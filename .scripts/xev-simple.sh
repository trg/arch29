#! /bin/bash
exec xev -geometry 800x800+5+29| awk -F'[ )]+' '/^KeyPress/ { a[NR+2] } NR in a { printf "%-3s %s\n", $5, $8 }'
