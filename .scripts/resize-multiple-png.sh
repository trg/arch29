#!/bash/sh

SIZES=(256 128 64 32)

for FILE in *.svg; do
  for SIZE in ${SIZES[*]}; do
    inkscape --export-png="${FILE%.svg}.${SIZE}x${SIZE}.png" --export-area-page --export-width=${SIZE} --export-height=${SIZE} --without-gui "${FILE}";
  done;
done
