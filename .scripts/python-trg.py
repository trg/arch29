#!/usr/bin/env python3


import tkinter as tk

root = tk.Tk()

root.title('tkinter')

# root.geometry('600x400+50+50')

window_width = 300
window_height = 200

# get the screen dimension
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()

# find the center point
center_x = int(screen_width/2 - window_width / 2)
center_y = int(screen_height/2 - window_height / 2)

# set the position of the window to the center of the screen
root.geometry(f'{window_width}x{window_height}+{center_x}+{center_y}')


greeting = tk.Label(text="Now we are cookin with GAS!!")
greeting.pack()

root.mainloop()

