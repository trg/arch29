# bash functions

# extract

ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# find

findit ()
{
    find / 2> /dev/null | grep "$1"
}

# permissions

octal()
{
    stat -c '%a' "$1"
}

# sudo vim

function sim()
{
    sudo vim "$*"
}

mkexe ()
{
    touch "$1" && chmod +x "$1" && nvim "$1"
}

mkcd ()
{
  mkdir -p -- "$1" && cd -P -- "$1"
}


# see last modification date of file
function lm()
{
    ls -l "$*" | awk '{print $6, $7, $8}'
}

function ca()
{
    config add "$*"
}
function cc()
{
    config commit -m "$1"
}


# ranger

function ranger-cd {
    local IFS=$'\t\n'
    local tempfile="$(mktemp -t tmp.XXXXXX)"
    local ranger_cmd=(
        command
        ranger
        --cmd="map Q chain shell echo %d > "$tempfile"; quitall"
    )

    ${ranger_cmd[@]} "$@"
    if [[ -f "$tempfile" ]] && [[ "$(cat -- "$tempfile")" != "$(echo -n `pwd`)" ]]; then
        cd -- "$(cat "$tempfile")" || return
    fi
    command rm -f -- "$tempfile" 2>/dev/null
}
#bind '"\C-o":"ranger-cd\C-m"'

 ranger () {
        local LOGFILE='/tmp/cd_ranger'
        # `command ranger` to launch ranger itself,
        # instead of causing an infinite loop with
        # this function calling itself
        command ranger "$@" || exit $?
        if [[ -f "${LOGFILE}" ]]
        then
                cd "$(cat "${LOGFILE}")"
                rm -f "${LOGFILE}"
        fi
}


up () {
  local d=""
  local limit="$1"

  # Default to limit of 1
  if [ -z "$limit" ] || [ "$limit" -le 0 ]; then
    limit=1
  fi

  for ((i=1;i<=limit;i++)); do
    d="../$d"
  done

  # perform cd. Show error if cd fails
  if ! cd "$d"; then
    echo "Couldn't go up $limit dirs.";
  fi
}
