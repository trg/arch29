#	╔╦╗╦═╗╔═╗  \│/  ╔═╗╔═╗╔╗╔╔═╗╦╔═╗  \│/  ╔╗ ╔═╗╔═╗╦ ╦
#	 ║ ╠╦╝║ ╦  ═╬═  ║  ║ ║║║║╠╣ ║║ ╦  ═╬═  ╠╩╗╠═╣╚═╗╠═╣
#	 ╩ ╩╚═╚═╝  /│\  ╚═╝╚═╝╝╚╝╚  ╩╚═╝  /│\  ╚═╝╩ ╩╚═╝╩ ╩
#!/bin/sh

function path(){
    old=$IFS
    IFS=:
    printf "%s\n" $PATH
    IFS=$old
}

export PANEL_HEIGHT="400"

export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}
export XDG_CONFIG_DIRS="/etc/xdg"

export PATH="$HOME/.cabal/bin:$PATH"
export PATH="$HOME/.cargo/bin:$PATH"
export PATH="$PATH:$GEM_HOME/bin"
export PATH="$PATH:$HOME/go/bin"
export PATH="$HOME/.local/bin:$PATH"

export PATH="$HOME/.config/dusk/scripts:$PATH"
export PATH="$HOME/.config/dusk/scripts/tools:$PATH"
export PATH="$HOME/.config/dusk/scripts/volume:$PATH"
export PATH="$HOME/.config/dusk/scripts/statusbar:$PATH"
export PATH="$HOME/.config/dusk/scripts/menu:$PATH"

export PATH="$HOME/.scripts:$PATH"
export PATH="$HOME/workspace/learning:$PATH"
export PATH="$HOME/workspace/yad:$PATH"
#export PATH="$PATH:$(du "/home/trg/workspace/" | cut -f2 | tr '\n' ':' | sed 's/:*$//')"

export CARGO_HOME="${XDG_DATA_HOME:-$HOME/.local/share}/cargo"
export DOOMDIR="${XDG_CONFIG_HOME:-$HOME/.config}/doom"
export DOTBARE_DIR="$HOME/.cfg"
export DOTBARE_TREE="$HOME"
export MOZ_USE_XINPUT2=1
export FZF_DEFAULT_COMMAND="fd . $HOME"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_ALT_C_COMMAND="fd -t d . $HOME"
#export GNUPGHOME="${XDG_DATA_HOME:-$HOME/.local/share}/gnupg"
export GOPATH="${XDG_DATA_HOME:-$HOME/.local/share}/go"
export HISTFILE="${XDG_DATA_HOME:-$HOME/.local/share}/history"
export INKSCAPE_PROFILE_DIR="${XDG_CONFIG_HOME:-$HOME/.config}/inkscape"

export KITTY_CONFIG_DIRECTORY="${XDG_CONFIG_HOME:-$HOME/.config}/kitty.d"
export KITTY_CACHE_DIRECTORY="${XDG_CACHE_HOME:-$HOME/.cache/kitty}"
#export QT_STYLE_OVERRIDE=kvantum
export LESSHISTFILE="-"
#export MOZ_LEGACY_HOME="${XDG_CONFIG_HOME:-$HOME/.config/}mozilla"
#export MOZ_PROFILE_HOME="${XDG_CONFIG_HOME:-$HOME/.config/}mozilla"
#export NPM_CONFIG_PREFIX="$HOME/.local"
export NPM_CONFIG_USERCONFIG="${XDG_CONFIG_HOME:-$HOME/.config/}/npm/npmrc"
export NVM_COLORS='cmgRY'
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

export GEM_PATH="${XDG_DATA_HOME:-$HOME/.local/share}/gem/ruby/3.0.0/bin"
#export GEM_HOME="$(ruby -e 'puts Gem.user_dir')"
#export GEM_SPEC_CACHE=/Users/myusername/Tools/.rbenv/versions/2.1.0/lib/ruby/gems/specs
export QT_QPA_PLATFORMTHEME="qt5ct"
#export PATH=\"$(pwd)/git-fuzzy/bin:\$PATH\
#export PASSWORD_STORE_DIR="${XDG_DATA_HOME:-$HOME/.local/share}/password-store"
export STARSHIP_CONFIG="${XDG_CONFIG_HOME:-$HOME/.config}/starship/starship.toml"
export STARSHIP_CACHE="${XDG_CACHE_HOME:-$HOME/.cache/starship}"

#export SUDO_ASKPASS="/usr/lib/ssh/x11-ssh-askpass"
export SUDO_ASKPASS="/usr/local/libexec/x11-ssh-askpass"
#export SSH_AGENT_PID=""
#export SSH_AUTH_SOCK="${XDG_RUNTIME_DIR}/gnupg/S.gpg-agent.ssh"
#export SUDO_ASKPASS="/usr/bin/xaskpass"
export TERMINFO="${XDG_DATA_HOME:=$HOME/.local/share}/terminfo"
#export TERMINFO_DIRS="$XDG_DATA_HOME"/terminfo:/usr/share/terminfo
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
export VLC_PLUGIN_PATH=/usr/lib/vlc/plugins
export XCURSOR_PATH="~/.local/share/icons"
export XMONAD_DATA_HOME="${XDG_DATA_HOME:-$HOME/.local/share}/xmonad"
export XMONAD_CACHE_HOME="${XDG_CACHE_HOME:-$HOME/.cache/}xmonad"
export XMONAD_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/.config/}xmonad"
#export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
export WGETRC="${XDG_CONFIG_HOME:-$HOME/.config}/wget/wgetrc"
export ALTERNATE_EDITOR="nvim"
export BROWSER="firefox"
export DESKTOP_SESSION="dusk"
export EDITOR="nvim"
export FRAMEBUFFER="/dev/fb0"
export GDK_DPI_SCALE=0
export GDK_BACKEND=x11
export GDK_SCALE=1.3
export HISTCONTROL=ignoredups:erasedups
#export LC_ALL="en_US.UTF-8"
export MANPAGER="sh -c 'sed -e s/.\\\\x08//g | bat -l man -p'"
export PAGER="bat"
export READER="xpdf"
export SCREEN_SIZE="1366x768 ./scripts/xephyr"
export TERMINAL="urxvt"
export VIDEO="mpv"
#export VISUAL="emacsclient -c -a emacs"
export VISUAL="nvim"
export WORKON_HOME=~/.virtualenvs
#source /usr/bin/virtualenvwrapper_lazy.sh

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

complete -c man which

if [ -n "$RANGER_LEVEL" ]; then export PS1="[ranger]$PS1"; fi



source ~/bin/remind

#PS1='\[\e[0;94m\]\w \[\e[0;38;5;118m\]$(git branch 2>/dev/null | grep '"'"'^*'"'"' | colrm 1 2) \n\[\e[0;3;38;5;202m\]\u \[\e[0;38;5;200m\]> \[\e[0m\]'

#export PS1="\[\033[38;5;4m\]\W\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;3m\]\$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/')\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;208m\]>\[$(tput sgr0)\] \[$(tput sgr0)\]"


export PS1="\[\033[38;5;128m\]\W\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;46m\]\$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/')\[$(tput sgr0)\]\n \[$(tput sgr0)\]\[\033[38;5;11m\]\u \[$(tput sgr0)\] \[$(tput sgr0)\]"


#[ -n "$PS1" ] && . ~/.bash_profile;

# history
HISTSIZE=5000
HISTFILESIZE=5000
#HISTFILE="$HOME/.bash_history"
#export HISTSIZE HISTFILESIZE HISTFILE
#echo HISTSIZE is $HISTSIZE
#echo HISTFILESIZE is $HISTFILESIZE
#echo HISTFILE is $HISTFILE



# standardizing your editing commands
set -o vi


# keybinds

# If not running interactively, don't do anything
if [ -f ~/.bash_aliases ]; then
. ~/.bash_aliases
fi

for _entry in /usr/lib/ruby/gems/*; do
  if [[ -d $_entry ]]; then
    # TODO: Add only if not there already
    export GEM_PATH=$_entry:$GEM_PATH
  fi
done
unset _entry


case ${TERM} in
  xterm*|rxvt*|Eterm*|aterm|kterm|gnome*|alacritty|st|kitty|konsole*)
    PROMPT_COMMAND='echo -ne "\033]0;${USER}${HOSTNAME%%.*}:${PWD/#$HOME/\~}\007"'
        ;;
  screen*)
    PROMPT_COMMAND='echo -ne "\033_${USER}${HOSTNAME%%.*}:${PWD/#$HOME/\~}\033\\"'
    ;;
esac
#export PS1="\e[32m\]\[\e[m\]\[\e[35m\]\u\[\e[m\]\[\e[34m\]@\[\e[m\]\[\e[32m\]\h\[\e[m\]\[\e[36m\]\w\[\e[m\]\[\e[32m\]\[\e[m\\$ "
#
cd ~


#eval `keychain --eval id_rsa --quiet`

#####################################################3
#export PYTHONPATH=/usr/bin/python3
#export PYENV_ROOT="$HOME/.pyenv"
#command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
#eval "$(pyenv init -)"

[[ -f ~/.bash_functions ]] && . ~/.bash_functions

[ -f "/home/trg/.ghcup/env" ] && source "/home/trg/.ghcup/env" # ghcup-env


#export GTK2_RC_FILES="${XDG_CONFIG_HOME:-$HOME/.config}/gtk-2.0/gtkrc-2.0"
# export GTK2_RC_FILES="$HOME/.gtk-2.0"
# FZF 
export FZF_MARKS_CMD="$HOME/.config/fzf/bin/fzf"
export FZF_MARKS_FILE="$HOME/.config/fzf/plugin/.fzf_marks"

export FZF_FZM_OPTS="--reverse --height 100% --min-height 30 --cycle +m --ansi --bind=ctrl-o:accept,ctrl-t:toggle --select-1 \
    --color='fg:#005FFF,fg+:#18E406,bg:#04010D,preview-bg:#D700D7,border:#D700D7'"

export FZF_DMARK_OPTS="--reverse --height 100% --min-height 30 --cycle +m --ansi --bind=ctrl-o:accept,ctrl-t:toggle --select-1 \
    --color='fg:#005FFF,fg+:#18E406,bg:#04010D,preview-bg:#D700D7,border:#D700D7'"


export FZF_COMPLETION_OPTS='--border --info=inline'

if [ -x "$(command -v fzf)"  ]
        then
            source /usr/share/fzf/key-bindings.bash
        fi
#export FZF_DEFAULT_OPTS=" \
#    --border rounded \
#    --reverse \
#    --height 75% \
#    --min-height 30 \
#    --cycle -m \
#    --ansi \
#    --bind=ctrl-o:accept,ctrl-t:toggle"

#    --color='bg:237,bg+:236,info:143,border:240,spinner:108' \
#        --color='hl:65,fg:252,header:65,fg+:252' \
#        --color='pointer:161,marker:168,prompt:110,hl+:108'

# First load fzf stuff as usual.
[ -f $HOME/.config/fzf/plugin/fzf-bash ] && source $HOME/.config/fzf/plugin/fzf-bash
#[ -f $HOME/.config/fzf/plugin/fzf-bash-completion ] && source $HOME/.config/fzf/plugin/fzf-bash-completion
[ -f $HOME/.config/fzf/plugin/fzf-bash-history ] && source $HOME/.config/fzf/plugin/fzf-bash-history
[ -f $HOME/.config/fzf/fzf-ranger-opts ] && source $HOME/.config/fzf/fzf-ranger-opts
[ -f $HOME/.config/fzf/fzf-completion-opts ] && source $HOME/.config/fzf/fzf-completion-opts
# Then configure and load this plugin.
FZF_CTRL_R_EDIT_KEY=ctrl-e
FZF_CTRL_R_EXEC_KEY=enter



shopt -s autocd # change to named directory
shopt -s cdspell # autocorrects cd misspellings
shopt -s cmdhist # save multi-line commands in history as single line
shopt -s dotglob
shopt -s histappend # do not overwrite history
shopt -s expand_aliases # expand aliases
shopt -s checkwinsize # checks term size when bash regains control

bind "set completion-ignore-case on"
bind -m vi-command 'Control-l: clear-screen'
bind -m vi-insert 'Control-l: clear-screen'
bind -x '"\C-p": __fzf_history__'

bind -x '"\t": fzf_bash_completion'

. "/home/trg/.local/share/cargo/env"


#eval "$(starship init bash)"
#[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
